<?php

/**
 * @file
 * Install functions for the Multiversion sequence filter module.
 */

use Drupal\Core\Config\FileStorage;

/**
 * Implements hook_schema().
 */
function multiversion_sequence_filter_schema() {

  $schema['multiversion_sequence_filter_index'] = [
    'description' => 'Stores a filtered sequence index.',
    'fields' => [
      'workspace_id' => [
        'description' => 'The associated workspace.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 1,
      ],
      'name' => [
        'description' => 'The name or key of entries.',
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'default' => '',
      ],
      'type' => [
        'description' => 'The entity type and bundle of the entry, concatenated by a point.',
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'default' => '',
      ],
      'seq' => [
        'description' => 'The sequence number.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'size' => 'big',
      ],
      'value' => [
        'description' => 'The value.',
        'type' => 'blob',
        'not null' => TRUE,
        'size' => 'big',
      ],
    ],
    'primary key' => ['workspace_id', 'name'],
    'indexes' => [
      'type' => ['type'],
      'seq' => ['seq'],
    ],
  ];

  $schema['multiversion_sequence_filter_values'] = [
    'description' => 'Keeps track of filter values for an entry.',
    'fields' => [
      'workspace_id' => [
        'description' => 'The associated workspace.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 1,
      ],
      'name' => [
        'description' => 'The name or key of an entry.',
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'default' => '',
      ],
      'filter_value' => [
        'description' => 'The filter value associated with the entry.',
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'default' => '',
      ],
      'deleted' => [
        'description' => 'Whether the filter value has been deleted already from the entity.',
        'type' => 'int',
        'size' => 'tiny',
        'default' => '0',
      ],
    ],
    'primary key' => ['workspace_id', 'name', 'filter_value'],
    'indexes' => [
      'deleted' => ['deleted'],
    ],
  ];

  $schema['multiversion_sequence_filter_additions'] = [
    'description' => 'Keeps track of additional entries to add for a given entry - even if not matching the filter.',
    'fields' => [
      'workspace_id' => [
        'description' => 'The associated workspace.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 1,
      ],
      'name' => [
        'description' => 'The name or key of an entry.',
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'default' => '',
      ],
      'additional_entry' => [
        'description' => 'The name of the additional entry.',
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'default' => '',
      ],
    ],
    'primary key' => ['workspace_id', 'name', 'additional_entry'],
    'indexes' => [
      'join_entries' => ['workspace_id', 'additional_entry'],
    ],
  ];

  return $schema;

}

/**
 * Add in new config.
 */
function multiversion_sequence_filter_update_8001() {
  // Install added config.
  $source = new FileStorage(__DIR__ . '/config/install');
  $config_storage = \Drupal::service('config.storage');
  foreach ($source->listAll() as $config_name) {
    $config_storage->write($config_name, $source->read($config_name));
  }
}

/**
 * Add deleted column to filter table.
 */
function multiversion_sequence_filter_update_8002() {
  $spec = [
    'deleted' => [
      'description' => 'Whether the filter value has been deleted already from the entity.',
      'type' => 'int',
      'size' => 'tiny',
      'default' => '0',
    ],
  ];
  $keys = [
    'indexes' => [
      'deleted' => ['deleted'],
    ]
  ];
  \Drupal::database()
    ->schema()
    ->addField('multiversion_sequence_filter_values', 'deleted', $spec['deleted']);

  \Drupal::database()
    ->schema()
    ->addIndex('multiversion_sequence_filter_values', 'deleted', ['deleted'], ['fields' => $spec] + $keys);

}
